import Customers from "./pages/Customers/Customers";
import Home from "./pages/HomePage/Home";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Contacts from "./pages/Contacts/Contacts";
import CreatOffer from "./pages/CreatOffer/CreatOffer";
import Test from "./pages/Test/Test";
import SearchOffer from "./pages/SearchOffer/SearchOffer";
import OfferDetails from "./pages/OfferDetails/OfferDetails";
import Profile from "./pages/Profile/Profile";
import AdminPanel from "./pages/AdminPanel/AdminPanel";
import MyOffers from "./pages/MyOffers/MyOffers";

function App() {
  return (
    <Router>
      <Routes>
        <Route exact path="/" element={<Home />}></Route>
        <Route exact path="/ourCustomers" element={<Customers />}></Route>
        <Route exact path="/contacts" element={<Contacts />}></Route>
        <Route exact path="/creatOffer" element={<CreatOffer />}></Route>
        <Route exact path="/searchOffer" element={<SearchOffer />}></Route>
        <Route exact path="/offerDetails" element={<OfferDetails />}></Route>
        <Route exact path="/profile" element={<Profile />}></Route>
        <Route exact path="/adminPanel" element={<AdminPanel />}></Route>
        <Route exact path="/myOffers" element={<MyOffers />}></Route>
      </Routes>
    </Router>
  );
}

export default App;
