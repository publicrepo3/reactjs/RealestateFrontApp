import React from "react";
import about from "./about.css";
import ReadMoreReact from 'read-more-react';

const About = () => {
  return (
    <div className="about-container">
      <div className="left-container">
        <p>WELCOME TO THE</p>
        <h1 className="h1">Best Real Estate Platform</h1>
        <p>
          Aparto is a full-service, luxury real estate brokerage and lifestyle
          company representing clients worldwide in a broad spectrum of classes,
          including residential, new development, resort real estate,
          residential leasing and luxury vacation rentals. Since our inception
          in 2011, we have redefined the business of real estate, modernizing
          and advancing the industry by fostering a culture of partnership, in
          which all clients and listings are represented by our agents.
        </p>
      </div>

      <div className="right-container">
        <img
          className="image"
          src="https://homesnappers-comms.s3.us-east-2.amazonaws.com/public/portfolio/high/portfolio-15.jpg"></img>
      </div>
    </div>
  );
};

export default About;
