import React from 'react'
import contactsUs from "./contactsUs.css"

const ContactsUs = () => {
  return (
      <div className="contactsUsContainer">
          <div className="contactsUsContainerLeft">
              <div className="contactsUsContainerLeftBlock">
                  <h2 className="contactsUsContainerLeftBlockH2">Address</h2>
                  <p className="contactsUsContainerLeftBlockP">Kafarsousah,damascuse,syria</p>
              </div>

              <div className="contactsUsContainerLeftBlock">
                  <h2 className="contactsUsContainerLeftBlockH2">Reservations</h2>
                  <p className="contactsUsContainerLeftBlockP">info@demolink.org</p>
              </div>

              <div className="contactsUsContainerLeftBlock">
                  <h2 className="contactsUsContainerLeftBlockH2">Front Desk</h2>
                  <p className="contactsUsContainerLeftBlockP">Toll Free phone number:+ 963 997286380</p>
              </div>

          </div>
          <div className="contactsUsContainerRight">
              <div className="contactsUsContainerRightTop">
              <h2 className="contactsUsContainerLeftBlockH2">Contact Form</h2>
              </div>
              <div className="contactsUsContainerRightBottom">
                  <form>
                      <input placeholder='First Name' type="text" className="inputForm" required="required"></input>
                      <input placeholder='Last Name' type="text" className="inputForm" required="required"></input>
                      <input placeholder='Email' type="email" className="inputForm" required="required"></input>
                      <input placeholder='Phone' type="number" className="inputForm" required="required"></input>
                      <input placeholder='Message' type="text" className="inputForm" required="required"></input>
                      <button className="buttonForm">SUBMIT</button>
                  </form>
              </div>
          </div>
    </div>
  )
}

export default ContactsUs