import { Email, LocationCity, Phone } from "@material-ui/icons";
import React from "react";
import footer from "./footer.css";

const Footer = () => {
  return (
    <div className="footerContainer">
      <div className="footerContainerLeft">
        <h2 className="footerLogo">APORTO</h2>
        <p className="footerP">
          Aparto is a unique real estate agency located in Indianapolis, IN. We
          strive to find the perfect homes for a variety of clients, including
          those who just moved to our country. If you're looking to buy, sell,
          or rent a residential property and don’t want to waste your money,
          look no further than Aparto Real Estate Agency!
        </p>
      </div>
          <div className="footerContainerRight">
              <div><h3>CONTACTS</h3></div>
              <div className="contactElement"><LocationCity /><span className="span">Kafarsousah,damascuse,syria</span></div>
              <div className="contactElement"><Phone /><span className="span">+963997286380</span></div>
              <div className="contactElement"><Email/><span className="span">info@aporto.com</span></div>
      </div>
    </div>
  );
};

export default Footer;
