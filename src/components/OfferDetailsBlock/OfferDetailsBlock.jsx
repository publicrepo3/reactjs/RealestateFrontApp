import React, { useEffect, useRef, useState } from "react";
import { useSelector } from "react-redux";
import { publicRequest, userRequestWithMultiPart } from "../../requestMethods";
import offerDetailsBlock from "./offerDetailsBlock.css";
import { useAlert } from "react-alert";

const OfferDetailsBlock = ({ prop }) => {
  const [appointment, setAppointment] = useState(false);
  const userId = useSelector((state) => state.user?.currentUser?.response);
  const offerId = prop.offer.offerId;
  const offer = prop.offer.offerData;
  const offerUserId = prop.offer.offerData.offerUserId;
  const [available, setAvailable] = useState(offer.isAvailable);
  const [style, setStyle] = useState("offerDetailsBlockContainerBottomLeftBottomButton1");
  const date = useRef();
  const time = useRef();
  const appointmentDetails = useRef(new FormData());
  const alert = useAlert();

  const handleClickForSetAppointment = async () => {
   
    appointmentDetails.current.append("userId", userId);
    appointmentDetails.current.append("offerId", offerId);
    appointmentDetails.current.append("appointmentDate", date.current);
    appointmentDetails.current.append("appointmentTime", time.current);
   
    try {
      const res = await userRequestWithMultiPart.post("MakeAnAppointment", appointmentDetails.current);
      console.log(res);
      setAppointment(false);
      alert.show("Appointment confirmed");
    } catch (error) {
      console.log(error);
    }
  }
  
  const handleClickForAvailable = () => {

  setAvailable(!offer.isAvailable);
  if (style === "offerDetailsBlockContainerBottomLeftBottomButton1")
    setStyle("offerDetailsBlockContainerBottomLeftBottomButton2");
  else
    setStyle("offerDetailsBlockContainerBottomLeftBottomButton1");
  }
  
  const isOfferAvailable = async() => {
    try {
      const res = await publicRequest.post("isAvailable", available);
      console.log(res);
    } catch (error) {
      console.log(error)
    }
  }
  useEffect(() => {
   // isOfferAvailable();
  }, [available])
  
  return (
    <div className="offerDetailsBlockContainer">
      <div className="offerDetailsBlockContainerTop">
        <div className="offerDetailsBlockContainerTopTitle">
          <h2 className="offerDetailsBlockContainerTopTitleH2">THE DETAILS</h2>
        </div>
        <div className="offerDetailsBlockContainerTopImg">
          <img
            src={offer.offerImageLink}
            alt=""
            className="offerDetailsBlockContainerTopImage"></img>
        </div>
        <div className="offerDetailsBlockContainerTopPara">
          <p className="offerDetailsBlockContainerTopP">
            You can find here all the details to help you choose what you want{" "}
          </p>
        </div>
      </div>

      <div className="offerDetailsBlockContainerBottom">
        <div className="offerDetailsBlockContainerBottomLeft">
          <div className="offerDetailsBlockContainerBottomLeftTop">
            <h4 className="offerDetailsBlockContainerBottomLeftTopH4">
              OfferFeatures
            </h4>
            <span className="offerDetailsBlockContainerBottomLeftTopSpan">
              <b>Tilte :</b> {offer.offerTitle}
            </span>
            <span className="offerDetailsBlockContainerBottomLeftTopSpan">
              <b>Located in : </b>
              {offer?.offerAddressDetails}
            </span>
            <span className="offerDetailsBlockContainerBottomLeftTopSpan">
              <b>For Sale :</b> {offer.offerServiceType.forSale ? "yes" : "no"}
            </span>
            <span className="offerDetailsBlockContainerBottomLeftTopSpan">
              <b>For Rent :</b> {offer.offerServiceType.forRent ? "yes" : "no"}
            </span>
            <span className="offerDetailsBlockContainerBottomLeftTopSpan">
              <b>Is available :</b> yes
            </span>
            <span className="offerDetailsBlockContainerBottomLeftTopSpan">
              <b>Description :</b>
              {offer.offerDesc}
            </span>
          </div>
          <div className="offerDetailsBlockContainerBottomLeftBottom">
            {userId === offerUserId ? (
             
               <button
                className={style}
                  onClick={handleClickForAvailable}>
                {" "}
                {available ? "Make it not available" :"Make it available"}
                </button>
              
            ) : (
              <button
                className="offerDetailsBlockContainerBottomLeftBottomButton"
                onClick={() => setAppointment(true)}>
                {" "}
                Take Appointment
              </button>
            )}
            {appointment ? (
              <>
                <input
                  className="offerDetailsBlockContainerBottomLeftBottomDate"
                  type="date" onChange={(e)=> date.current = e.target.value}></input>
                <input
                  className="offerDetailsBlockContainerBottomLeftBottomDate"
                  type="time" onChange={(e)=> time.current = e.target.value}></input>
                <button
                  className="offerDetailsBlockContainerBottomLeftBottomButtonDone"
                  onClick={handleClickForSetAppointment}>
                  Done
                </button>
              </>
            ) : null}
          </div>
        </div>
        <div className="offerDetailsBlockContainerBottomRight">
          <img
            className="offerDetailsBlockContainerBottomRightImg"
            src={offer.offerCertificateLink}
            alt=""></img>
        </div>
      </div>
    </div>
  );
};

export default OfferDetailsBlock;
