import React, { useRef, useState } from "react";
import { publicRequest } from "../../requestMethods";
import offerDetailsForAdmin from "./offerDetailsForAdmin.css";

const OfferDetailsForAdmin = ({ prop }) => {
  
  const IsApproved = useRef(prop.offerData.isAdminApproved);
  const [accept, setAccept] = useState(IsApproved.current);

  const handleClickForAvailable = async () => {
    if (accept == "true") {
      setAccept("false");
    } else {
      setAccept("true");
    }

    try {
      const res = await publicRequest.get(
        `Admin/ApproveOffer?offerId=${prop.offerId}&isAdminApproved=${accept}`
      );
    

      console.log(res);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className="adminPanelContainerElement">
      <div className="adminPanelContainerElementLeft">
        <span className="adminPanelContainerElementLeftSpan">
          <b>Title :</b> {prop.offerData.offerTitle}
        </span>
        <span className="adminPanelContainerElementLeftSpan">
          <b>Address :</b> {prop.offerData.offerAddressDetails}
        </span>
        <span className="adminPanelContainerElementLeftSpan">
          <b>For Sale :</b>{" "}
          {prop.offerData.offerServiceType.forSale ? "yes" : "no"}
        </span>
        <span className="adminPanelContainerElementLeftSpan">
          <b>For Rent :</b>{" "}
          {prop.offerData.offerServiceType.forRent ? "yes" : "no"}
        </span>
        <span className="adminPanelContainerElementLeftSpan">
          <b>Description :</b> {prop.offerData.offerDesc}
        </span>

        {accept == "true" ? (
          <div className="adminPanelContainerElementLeftButtonContainer">
            <button
              className="offerDetailsBlockContainerBottomLeftBottomButton2"
              onClick={handleClickForAvailable}>
              Reject Offer
            </button>
          </div>
        ) : (
          <div className="adminPanelContainerElementLeftButtonContainer">
            <button
              className="offerDetailsBlockContainerBottomLeftBottomButton1"
              onClick={handleClickForAvailable}>
              Accept Offer
            </button>
          </div>
        )}
      </div>

      <div className="adminPanelContainerElementRight">
        <img
          src={prop.offerData.offerImageLink}
          alt=""
          className="adminPanelContainerElementRightImg"
        />
        <img
          src={prop.offerData.offerCertificateLink}
          alt=""
          className="adminPanelContainerElementRightImg"
        />
      </div>
    </div>
  );
};

export default OfferDetailsForAdmin;
