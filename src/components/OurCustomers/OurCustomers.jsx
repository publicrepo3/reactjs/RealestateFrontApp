import React from "react";
import ourCustomers from "./ourCustomers.css";

const OurCustomers = () => {
  return (
    <div className="customerContainer">
      <div className="customerContainerTop">
        <p className="customerP">TESTIMONIAL</p>
        <h1 className="customerH1">What Our Clients Say</h1>
          </div>
          <hr/>
      <div className="customerContainerBottom">
        <div className="customerContainerBottomElement">
          <div className="elementLeft">
            <div className="clientInfo">
              <img
                className="clientImg"
                src="https://tswears.com/wp-content/uploads/2021/05/180426470.jpg"></img>
              <span className="clientName">M.Ali</span>
            </div>
            <div className="clientTestimonial">
              <p className="clientP">
                Diam amet eos at no eos sit, amet rebum ipsum clita stet, diam
                sea est diam eos, sit vero stet justo
              </p>
            </div>
          </div>
                  <div className="elementRight">
                      <img className="clientHome" src="https://www.realestate.com.au/blog/images/1024x768-fit,progressive/2017/05/28191805/capi_ba3eccf446b6a03c4f5e74c7c10afaee_8cb35f3249628ab76c600c65c920d2e8.jpeg"></img>
          </div>
              </div>
              
              <div className="customerContainerBottomElement">
                  <div className="elementLeft">
                      
            <div className="clientInfo">
              <img
                className="clientImg"
                src="http://socialable.co.uk/wp-content/uploads/2015/03/face_2.jpg"></img>
              <span className="clientName">M.Mohammad</span>
            </div>
            <div className="clientTestimonial">
              <p className="clientP">
                Diam amet eos at no eos sit, amet rebum ipsum clita stet, diam
                sea est diam eos, sit vero stet justo
              </p>
            </div>
          </div>
                  <div className="elementRight">
                      <img className="clientHome" src="https://i.pinimg.com/550x/54/7b/2b/547b2b6595611c2f836cbd6c70c5a24d.jpg"></img>
          </div>
              </div>
              
              <div className="customerContainerBottomElement">
          <div className="elementLeft">
            <div className="clientInfo">
              <img
                className="clientImg"
                src="https://www.bidsketch.com/blog/wp-content/uploads/2017/03/Screen-Shot-2017-03-30-at-8.46.02-PM.png"></img>
              <span className="clientName">M.Selen</span>
            </div>
            <div className="clientTestimonial">
              <p className="clientP">
                Diam amet eos at no eos sit, amet rebum ipsum clita stet, diam
                sea est diam eos, sit vero stet justo
              </p>
            </div>
          </div>
                  <div className="elementRight">
                      <img className="clientHome" src="https://www.archdez.com/wp-content/uploads/2021/02/Regus-Real-Estate-Interior-Design-Office-7.jpg"></img>
          </div>
        </div>
      </div>
    </div>
  );
};

export default OurCustomers;
