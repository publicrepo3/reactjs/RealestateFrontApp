import services from "./services.css";
import React from "react";

const Services = () => {
  return (
    <div className="servicesContainer">
      <div className="servicesContainerTop">
        <p className="para1">A LITTLE EXTRA...</p>
        <h1>Featured Offer</h1>
        <p className="para1">
          Our agency offers a variety of additional services to our clients,
          from discounts on furniture and household materials to some special
          deals from our partners. Below is one of the most popular offers from
          us
        </p>
      </div>
      <div className="servicesContainerBottom">
        <div className="servicesContainerBottomLeft">
          <img className="imageServices" src="https://www.realestatepropertiessantafe.com/wp-content/uploads/2014/06/homesearch.jpg"></img>
        </div>
        <div className="servicesContainerBottomRight">
          <h2>Rent a New Apartment Get FREE Furniture</h2>
          <p className="para1">
            That's correct! We're so determined to make your experience at our
            properties better that we will even provide you with free furniture
            from the store of your choice!
          </p>
          <button className="servicesContainerBottomRightButton">
            Add Offers
          </button>
        </div>
      </div>
    </div>
  );
};

export default Services;
