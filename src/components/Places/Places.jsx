import React from "react";
import places from "./places.css";

const Places = () => {
  return (
    <div className="placesContainer">
      <div className="placesContainerTop">
        <h1 className="placesH1">THE PLACES WE SERVE</h1>
      </div>
      <div className="placesContainerBottom">
        <div className="cityContainer">
          <img
            className="cityImag"
            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARMAAAC3CAMAAAAGjUrGAAAAhFBMVEUAAADOESb///8Aej3OABn//f4AfkAAeDkAbyUAcCgAdjUAdDEAcy0AcCcAdjTf7OX3+/mKtpvR49kAaxqBsZQjhE6Yv6cwiVbl8Oo7jl71+ve91sft9fFCkWNtpoTG3M+bwKpKmm9WmnEWgEenyLXY595rpoN4rY2y0L5cnne/18lNlmzHcj+EAAAEYklEQVR4nO2ba1faQBRFqdM6k0x4FEFArS98gP3//69JCERyAio5d7Vd6+yv6vGy70zmEe19E016f7uAfxA5QeQEkRNEThA5QeQEkRNEThA5QeQEkRNEThA5QeQEkRNEThA5QeQEkRNEThA5QeQEkRNEThA5QeQEkRNEThA5QeQEkRNEThA5QeQEkRNEThA5QeQEkRNEThA5QeQEkRNEThA5QeQEkRNEThA5QeQEkRNEThA5QeQEkRNEThA5QeQE6X0XTXo/RJPemWgiJ4icIHKCyAkiJ4ilk9F/mm3pZPKfZls6CWOz6HEwizZ1Mr6wa+bkws63pZNJXJplL6Ph5DF0svTJs1H0c+LPjaLPLJ08Bxd/GmX/jC5Y+bZ0ktdt1sxzb+fb0snUO6tm5kPQ+alJdIGZk2Fet1Uz8yGY+x6aZJ8ZOrks6jZqZjEEDSePmZOybptmlkPQcPJYORkmRd0uXhpkl0PQucRq8lg5qer2M4PsmbfzXWDlZDN1nOvzmznvb6LNJo+Rk3nY1O3iPT37PlbZYU7PLjFysqvbYPJUU8fEd4mRk8W2bte/IUff9LfRfkGOrmA4uXrIzhvslDjvGl8anH/lmD/Ov38f9y67+WuzhyvC5+GMk/uQ+j3cO/a/Eh6/VvZoHT4bnQbOSkSaO8/L6D6DD09fzn4K/uPg4vGyJJ2uaM+Tl/Bx1S5OT1mah9PPCA8vrI/Ce8ausvSDqn14PTH79cOhkmYr2ichrjs3i+x42fH65OzreFx4tiDuVahr8eRYO7NFl1X55tcR4T5Qb2e5+5OxP9ROf9H1aH95cUj4wN9Rqt9C3rONHpP2sl33dw/jPKWN5JH8UpC+j31KWtrZXzPKHq37GO0T+g6fv7cfzkBK+vVNSTsrmJp+xj94G5x3XqDwjJYND9qUtiup4TsZ4QJB2zusWrL5f2HAd3KNdadrUvYal7Xs9E3PIfhOWup2CaeZo5ZFjea7hu5k1LZgkprZMgTzZZ4+eehO3upe1usP6Un47uldZyf0yUN3UtedPMZd5Sllf7KL9rHeG/JXHrqT7fD2YXU23x3ykzdC9G4IFlcOq/5WOG+lr2A7uavqzmblQXV7yE9vCdm3aWW7vHKYzyr9Cfe0w3dyO9iUvT2ovg02HyQlZKel33SwfYBUx/ABw/d72E5KJWmsW3e1OeQTmrkZgtmv+srhbiN80Dl6H7KTsu7k99419GXRzvTUK7aa17QYgHvX0Fe/E4PJQ3aS1+1D86BaXmB3fxLm422wbF453OfCCb73IDvJV8m22/P8kN+5meOk9cohF+5dx+gG5Hu2cOD2fBVi12ZOsgPvQV4C+Y+TuU5ew6ET8HwWO2bH6aFr6OuT3we0w3Vy7Pb8ttvkuTuy4s65L46pTo6/9ez2Mv34TzNeE+/Q/+8gcoLICSIniJwgcoLICdITQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEJ35A5qE8GcBofKYAAAAAElFTkSuQmCC"></img>
          <span>Syria</span>
        </div>

        <div className="cityContainer">
          <img
            className="cityImag"
            src="https://i.pinimg.com/originals/a5/e3/ee/a5e3eea6beec536494fe63de17e2968f.jpg"></img>
          <span>Lebanon</span>
        </div>

        <div className="cityContainer">
          <img
            className="cityImag"
            src="https://www.countryaah.com/wp-content/uploads/2020/08/Flag-of-Egypt.jpg"></img>
          <span>Egypt</span>
        </div>

        <div className="cityContainer">
          <img
            className="cityImag"
            src="https://puntlandpost.net/wp-content/uploads/2021/05/2-1.png"></img>
          <span>Turkey</span>
        </div>

        <div className="cityContainer">
          <img
            className="cityImag"
            src="http://cdn.shopify.com/s/files/1/1263/1727/products/800px-Flag_of_Algeria__28bordered_29.svg_c1dd55a9-0f60-4124-9493-ba5d13c91ec3_grande.png?v=1583686251"></img>
          <span>Algeria</span>
        </div>
      </div>
    </div>
  );
};

export default Places;
