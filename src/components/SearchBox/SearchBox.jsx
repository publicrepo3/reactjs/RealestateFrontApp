import React, { useRef } from "react";
import { publicRequest, userRequestWithMultiPart } from "../../requestMethods";
import searchBox from "./searchBox.css";

const SearchBox = (props) => {

  const location = useRef();
  const space = useRef();
  const price = useRef();
  const rooms = useRef();
  const floor = useRef();
  const cladding = useRef();
  const fullOrder = useRef({});
  const orderAsString = useRef();
 

  const handleClick =  async() => {
    fullOrder.current = {
      location: location.current,
      space: space.current,
      price: price.current,
      rooms: rooms.current,
      floor: floor.current,
      cladding : cladding.current
    }
    orderAsString.current = JSON.stringify(fullOrder.current).replace("{","").replace("}" , "")

    try {
  console.log( orderAsString.current)
  const res = await publicRequest.get(`AdvancedSearchOfferRoute?offset=0&limit=10&keyWord=${orderAsString.current}`);
  console.log(res)
  
} catch (error) {
  console.log(error)
}
  }

  
  

  return (
    <div className="topSearchContainer">
      <div className="searchContainer">
        <div className="searchContainerElement">
          <span className="searchContainerElementLabel">Location</span>
          <input className="searchContainerElementInput" type="text" name = "location" onChange={(e)=>  location.current= e.target.value }></input>
        </div>
        <div className="hrLine"></div>
        <div className="searchContainerElement">
          <span className="searchContainerElementLabel">Space</span>
          <input className="searchContainerElementInput" type="number" name = "space" onChange={(e)=>space.current= e.target.value}></input>
        </div>
        <div className="hrLine"></div>
        <div className="searchContainerElement">
          <span className="searchContainerElementLabel">Price</span>
          <input className="searchContainerElementInput" type="number" name = "price" onChange={(e)=> price.current= e.target.value}></input>
        </div>
        <div className="hrLine"></div>
        <div className="searchContainerElement">
          <span className="searchContainerElementLabel">Room`s Number</span>
          <input className="searchContainerElementInput" type="number" name = "rooms" onChange={(e)=>rooms.current= e.target.value}></input>
        </div>
        <div className="hrLine"></div>
        <div className="searchContainerElement">
          <span className="searchContainerElementLabel">Floor</span>
          <input className="searchContainerElementInput" type="number" name = "floor" onChange={(e)=>floor.current=e.target.value}></input>
        </div>
        <div className="hrLine"></div>
        <div className="searchContainerElement">
          <span className="searchContainerElementLabel">Cladding level</span>
          <input className="searchContainerElementInput" type="text" name = "cladding" onChange={(e)=>cladding.current= e.target.value}></input>
              </div>
              
        <div className="searchContainerElement">
          <button className="searchContainerButton" onClick={handleClick}>Search</button>
        </div>
      </div>
    </div>
  );
};

export default SearchBox;
