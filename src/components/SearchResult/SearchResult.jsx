import React, { useRef, useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { publicRequest } from "../../requestMethods";
import searchResult from "./searchResult.css";

const SearchResult = (props) => {
  const [offers, setOffers] = useState([]);

  const getOffers = async () => {
    try {
      const res = await publicRequest.get(
        "Admin/GetAllOffers?offset=0&limit=50"
      );
      setOffers(res.data.response);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getOffers();
  }, []);

  console.log(props.searchedOffers)

  return (
    <div className="searchResultContainer">
      {offers?.map((offer) => (
        <div className="searchResultElement" key={offer.offerId}>
          <div className="searchResultElementLeft">
            <span className="searchResultElementLeftSpan">
              <b>Offer Title : </b> {offer.offerData.offerTitle}
            </span>
            <span className="searchResultElementLeftSpan">
              <b>Offer Location : </b>damas
            </span>
           <Link to='/offerDetails' state={{offer : {offer}}}> <button className="searchResultElementLeftButton">
              Show Offer Details
            </button>
            </Link>
          </div>
          <div className="searchResultElementRight">
            <img
              className="searchResultElementRightImg"
              src={offer.offerData.offerImageLink}
              alt=""></img>
          </div>
        </div>
      ))}
    </div>
  );
};

export default SearchResult;
