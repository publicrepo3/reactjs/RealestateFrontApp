import React, { useState } from "react";
import {
  ArrowLeftOutlined,
  ArrowRightOutlined,
  Opacity,
} from "@material-ui/icons";
import slider from "./slider.css";
import LoginModal from "react-login-modal";
import SimpleImageSlider from "react-simple-image-slider";
import { useDispatch, useSelector } from "react-redux";
import { seenLoginEnded } from "../../redux/seenLogin";
import { login } from "../../redux/apiCalls";
import {useNavigate} from 'react-router-dom';


const Slider = ({ prop }) => {
  const seen = useSelector((state) => state.seen?.currentState);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleSignup = (username, email, password) => {
    console.log({ userName: username, email: email, password: password });
    const user = {
      userName: username,
      userPassword: password,
      userEmail: email,
      userAddress: "The best address",
    userRoleType : ["1","2"],

    }
    dispatch(seenLoginEnded());
    login(dispatch, user);
navigate('/creatOffer')
  };
  const handleLogin = (username, password) => {
    console.log({ userName: username, password: password });
    const user = {
      userName: username,
      userPassword: password,
    }
    dispatch(seenLoginEnded());
    login(dispatch, user);
  };
  //const seen = false;
  console.log("seen from slider : " + seen);
  return (
    <div className="container">
      <SimpleImageSlider
        width="100%"
        height={600}
        images={prop}
        showBullets={true}
        showNavs={true}
        autoPlay={true}
        autoPlayDelay={10.0}
        style={{ opacity: "0.7" }}
      />
     
      <div className="loginContainer">
        {seen && (
          <LoginModal
            handleSignup={handleSignup}
            handleLogin={handleLogin}
            buttonColor={"#52AE64"}
            disabledButtonColor={"#C7E4CD"}
            buttonHoverColor={"#A7D5B0"}
            fontFamily={"roboto"}
            errorMessage={"Incorrect username or password"}
            errorEnable={true}
          />
        )}
      </div>
    </div>
  );
};

export default Slider;
