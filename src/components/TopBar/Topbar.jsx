import { Search, Settings } from "@material-ui/icons";
import React, { useState } from "react";
import { Link } from "react-router-dom";
import topbar from "./topbar.css";
import { useDispatch, useSelector } from "react-redux";
import { seenLoginSuccess, seenLoginStart } from "../../redux/seenLogin";
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

const linkStyle = {
  textDecoration: "none",
  color: "black",
};

const Topbar = () => {
  const user = useSelector((state) => state.user?.currentUser);

  const dispatch = useDispatch();

  //-----------------
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  
  const handleClick2 = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  //-------------------------

  const handleClick = () => {
    if (!user) {
      dispatch(seenLoginStart());
      dispatch(seenLoginSuccess());
    }
  };

  return (
    <>
      <div className="topContainer">
        <div className="leftSide">
          <h1 className="logo">APORTO</h1>
        </div>
        <div className="centerSide">
          <Link to="/" style={linkStyle}>
           
            <div className="centerElement">HOME</div>{" "}
          </Link>
          <Link to="/ourCustomers" style={linkStyle}>
            <div className="centerElement">OUR CUSTOMERS</div>
          </Link>
          <Link to="/contacts" style={linkStyle}>
            <div className="centerElement">CONTACTS</div>
          </Link>
          <Link to="/searchOffer">
          
            <div className="centerElement">
              <Search style={linkStyle} />
            </div>
          </Link>
        </div>
        <div className="rightSide">
          <Link to={user ? "/creatOffer" : "/"} style={{width:"150px"}}  state={{editOffer: false}}>
            <button className="rightButton" onClick={handleClick}>
              Add Offers
            </button>
          </Link>

          <div>
      <Button
        id="demo-positioned-button"
        aria-controls={open ? 'demo-positioned-menu' : undefined}
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        onClick={handleClick2}
      >
              <Settings style={{color : "gray"}}/>
      </Button>
      <Menu
        id="demo-positioned-menu"
        aria-labelledby="demo-positioned-button"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
      >
       <Link to="/profile" style={{textDecoration : "none" , color : "gray"}}> <MenuItem onClick={handleClose}>Profile</MenuItem></Link>
       <Link to="/AdminPanel" style={{textDecoration : "none" , color : "gray"}}> <MenuItem onClick={handleClose}>Admin panel</MenuItem></Link>
       
      </Menu>
          </div>
          

        </div>
      </div>
      <hr></hr>
    </>
  );
};

export default Topbar;
