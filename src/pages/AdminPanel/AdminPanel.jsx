import { Clear, Done } from "@material-ui/icons";
import React, { useEffect, useState } from "react";
import Footer from "../../components/Footer/Footer";
import OfferDetailsForAdmin from "../../components/OfferDetailsForAdmin/OfferDetailsForAdmin";
import Topbar from "../../components/TopBar/Topbar";
import { publicRequest } from "../../requestMethods";
import adminPanel from "./adminPanel.css";

const AdminPanel = () => {
  const [offers, setOffers] = useState([]);
  const getAllOffer = async () => {
    try {
      const res = await publicRequest.get(
        "Admin/GetAllOffers?offset=0&limit=10"
      );

      setOffers(res.data.response);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    getAllOffer();
  }, []);

  return (
    <>
      <Topbar />
      <div className="adminPanelContainer">
        {offers.map((offer) => (
          <OfferDetailsForAdmin prop={offer} key={offer.offerId}/>
        ))}
      </div>
      <Footer />
    </>
  );
};

export default AdminPanel;
