import React from 'react'
import ContactsUs from '../../components/ConactsUs/ContactsUs';
import Footer from '../../components/Footer/Footer';
import Topbar from '../../components/TopBar/Topbar';
import Slider from '../../components/Slider/Slider';

const Contacts = () => {

  const images = [
    {
      url: "https://www.farmworkersny.org/wp-content/uploads/2021/11/istockphoto-1311934969-170667a-1.jpg",
    },
    {
      url: "https://media.istockphoto.com/photos/contact-us-concept-icon-telephone-address-and-email-on-blue-3d-picture-id1312566254?k=20&m=1312566254&s=170667a&w=0&h=cEfFY2iExoJK9XoITR8KDYkVbJVFTFhOkJLIbfHeekM=",
    },
    {
      url: "https://www.thewomenachiever.com/wp-content/uploads/2021/12/istockphoto-1271752802-170667a.jpg",
    },
  ];

  return (
      <div>
      <Topbar />
      <Slider prop={images}/>
      <ContactsUs/>
          <Footer/>
    </div>
  )
}

export default Contacts