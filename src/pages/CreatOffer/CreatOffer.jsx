import React, { useRef } from "react";
import Footer from "../../components/Footer/Footer";
import Topbar from "../../components/TopBar/Topbar";
import creatOffer from "./creatOffer.css";
import { useSelector } from "react-redux";
import { publicRequest, userRequestWithMultiPart } from "../../requestMethods";
import { useLocation, useNavigate } from "react-router-dom";
import { Publish } from "@material-ui/icons";
import { useAlert } from "react-alert";

const CreatOffer = () => {
  const offerUserId = useSelector((state) => state.user?.currentUser?.response);
  const bodyFormData = useRef(new FormData());
  const title = useRef();
  const address = useRef();
  const desc = useRef();
  const offerType = useRef();
  const navigate = useNavigate();
  const alert = useAlert();
  const space = useRef();
  const price = useRef();
  const rooms = useRef();
  const floor = useRef();
  const cladding = useRef();
  const fullDesc = useRef();

  const handleChangeForTitle = (e) => {
    title.current = e.target.value;
  };

  const handleChangeForAdress = (e) => {
    address.current = e.target.value;
  };

  const handleChangeForDesc = (e) => {
    desc.current = e.target.value;
  };

  const handleChangeForType = (e) => {
    offerType.current = e.target.value;
  };

  const handleImg = async (e) => {
    bodyFormData.current.append("offerImage", e.target.files[0]);
  };

  const handleCertificate = async (e) => {
    bodyFormData.current.append("offerCertificate", e.target.files[0]);
  };

  const submitFunction = () => {

    fullDesc.current = {
     
      space: space.current,
      price: price.current,
      rooms: rooms.current,
      floor: floor.current,
      cladding : cladding.current
    }
    desc.current = JSON.stringify(fullDesc.current).replace("{", "").replace("}", "")
    
    bodyFormData.current.append("offerTitle", title.current);
    bodyFormData.current.append("offerAddressDetails", address.current);
    bodyFormData.current.append("offerDesc", desc.current);
    if (offerType.current == "Sale") {
      bodyFormData.current.append("forSale", true);
      bodyFormData.current.append("forRent", false);
    } else {
      bodyFormData.current.append("forSale", false);
      bodyFormData.current.append("forRent", true);
    }
    bodyFormData.current.append("offerUserId", offerUserId);
  };
  const handleClick = async (e) => {
    e.preventDefault();
    submitFunction();
    for (var pair of bodyFormData.current.entries()) {
      console.log(pair[0] + ", " + pair[1]);
    }
    try {
      const res = await userRequestWithMultiPart.post(
        "CreateOffer",
        bodyFormData.current
      );
      console.log(res);
      alert.show("Offer has been added successfully");
      navigate("/searchOffer");
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <>
      <Topbar />
      <div className="creatOfferContainer">
        <h2 className="offerH2">Offer Form</h2>

        <form>
          <input
            type="text"
            name="offerTitle"
            className="offerInputText"
            onChange={handleChangeForTitle}
            placeholder="Offer Title"></input>
          <input
            type="text"
            name="offerAddressDetails"
            className="offerInputText"
            onChange={handleChangeForAdress}
            placeholder="Offer Address"></input>

          <input
            type="text"
            name="offerType"
            className="offerInputText"
            onChange={handleChangeForType}
            placeholder="Sale or Rent"></input>

          <input
            className="offerInputText"
            type="text"
            name="space"
            placeholder="Space"
            onChange={(e) => (space.current = e.target.value)}></input>
          
          
          <input
            className="offerInputText"
            type="text"
            name="price"
            placeholder="Price"
            onChange={(e) => (price.current = e.target.value)}></input>
          
          
          <input
            className="offerInputText"
            type="text"
            name="romms"
            placeholder="Room`s Number"
            onChange={(e) => (rooms.current = e.target.value)}></input>
          
          
          <input
            className="offerInputText"
            type="text"
            name="floor"
            placeholder="Floor"
            onChange={(e) => (floor.current = e.target.value)}></input>
          
          
          <input
            className="offerInputText"
            type="text"
            name="cladding"
            placeholder="Cladding Level"
            onChange={(e) => (cladding.current = e.target.value)}></input>
          

          <input
            type="file"
            className="offerInputForImage"
            onChange={handleImg}></input>
          <input
            type="file"
            className="offerInputForCer"
            onChange={handleCertificate}></input>

          <button className="offerButton" onClick={handleClick}>
            CREATE OFFER
          </button>
        </form>
      </div>

      <Footer />
    </>
  );
};

export default CreatOffer;

/*
<textarea
            name="offerDesc"
            className="offerInputTextDesc"
            onChange={handleChangeForDesc}
            form="usrform"
            defaultValue="Write offer description such as price, floor, room`s number and Cladding level"></textarea>
*/
