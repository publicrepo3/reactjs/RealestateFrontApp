import React from 'react'
import Footer from '../../components/Footer/Footer';
import OurCustomers from '../../components/OurCustomers/OurCustomers';
import Slider from '../../components/Slider/Slider';
import Topbar from '../../components/TopBar/Topbar';

const Customers = () => {

    const images = [
        {
          url: "https://www.build-review.com/wp-content/uploads/2021/03/luxury-interior.jpg",
        },
        {
          url: "https://www.pagodared.com/blog/wp-content/uploads/2018/09/00-Summer_Thornton_Chicago_Co-op_Blue_Living.jpg",
        },
        {
          url: "https://media.placester.com/image/upload/c_fill,f_auto,q_80,w_2560/v1/inception-app-prod/YmNlYjgzOWUtOTAwNS00ZTljLTlhZDctNzM0MmRmZTU5MzQ0/favicon/2020/05/img3517.jpg",
        },
      ];
  return (
      <div>
          <Topbar />
      <Slider prop={images} />
      <OurCustomers/>
          <Footer/>
    </div>
  )
}

export default Customers