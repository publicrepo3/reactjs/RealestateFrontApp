import React from 'react'
import About from '../../components/AboutUs/About';
import Footer from '../../components/Footer/Footer';
import Services from '../../components/OurServices/Services';
import Places from '../../components/Places/Places';
import Slider from '../../components/Slider/Slider';
import Topbar from '../../components/TopBar/Topbar';

const Home = () => {
  const images = [
    {
      url: "https://images.squarespace-cdn.com/content/v1/52f50c98e4b08f72cd95680a/1391890145392-M4MTWA4ENFZRD89BOSLX/151_E_58_39E_LVRM_SBoardman.jpg?format=1000w",
    },
    {
      url: "https://images.squarespace-cdn.com/content/v1/50eb66f0e4b0033596299a41/1558035907292-LKD6CW8U3ZHVKCIZMRMX/sothebys-international-realty-toronto-luxury-homes",
    },
    {
      url: "https://images.realty.mx/9ea4811032b1371ad6e6dc99c1cbff4d/images/assets/54474_61757.jpg",
    },
  ];
  return (
      <div>
      <Topbar />
      <Slider prop={images}/>
      <About />
      <Services />
      <Places />
      <Footer/>
    </div>
  )
}

export default Home