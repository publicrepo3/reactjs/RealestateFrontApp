import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import Footer from "../../components/Footer/Footer";
import Topbar from "../../components/TopBar/Topbar";
import { publicRequest } from "../../requestMethods";
import myOffers from "./myOffers.css";

const MyOffers = () => {
  const [myOffers, setMyOffers] = useState([]);
  const userId = useSelector((state) => state.user.currentUser.response);

  const getMyOffers = async () => {
    try {
      const res = await publicRequest.get(
        `GetOffersByUserId?offerUserId=${userId}`
      );

      setMyOffers(res.data.response);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    getMyOffers();
  }, []);

  return (
    <>
      <Topbar />
      <div className="myOffersContainer">
        {myOffers.map((offer) => (
          <Link
            to="/offerDetails"
            state={{ offer: { offer } }}
            style={{ color: "black", textDecoration: "none" }}>
            <div className="myOffersElement" key={offer.offerId}>
              <div className="adminPanelContainerElementLeft">
                <span className="adminPanelContainerElementLeftSpan">
                  <b>Title :</b> {offer.offerData.offerTitle}
                </span>
                <span className="adminPanelContainerElementLeftSpan">
                  <b>Address :</b> {offer.offerData.offerAddressDetails}
                </span>
                <span className="adminPanelContainerElementLeftSpan">
                  <b>For Sale :</b>{" "}
                  {offer.offerData.offerServiceType.forSale ? "yes" : "no"}
                </span>
                <span className="adminPanelContainerElementLeftSpan">
                  <b>For Rent :</b>{" "}
                  {offer.offerData.offerServiceType.forRent ? "yes" : "no"}
                </span>
                <span className="adminPanelContainerElementLeftSpan">
                  <b>Description :</b> {offer.offerData.offerDesc}
                </span>
              </div>

              <div className="adminPanelContainerElementRight">
                <img
                  src={offer.offerData.offerImageLink}
                  alt=""
                  className="adminPanelContainerElementRightImg"
                />
                <img
                  src={offer.offerData.offerCertificateLink}
                  alt=""
                  className="adminPanelContainerElementRightImg"
                />
              </div>
            </div>
          </Link>
        ))}
      </div>
      <Footer />
    </>
  );
};

export default MyOffers;


