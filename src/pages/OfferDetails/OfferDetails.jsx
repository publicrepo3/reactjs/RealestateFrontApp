import React from 'react'
import { useLocation } from 'react-router-dom';
import Footer from '../../components/Footer/Footer';
import OfferDetailsBlock from '../../components/OfferDetailsBlock/OfferDetailsBlock';
import Topbar from '../../components/TopBar/Topbar';
import offerDetails from "./offerDetails.css"

const OfferDetails = () => {

    const location = useLocation();
    const offer = location.state?.offer;
    
  return (
      <>
          <Topbar />
          <OfferDetailsBlock prop={offer}/>
          <Footer/>
      </>
  )
}

export default OfferDetails