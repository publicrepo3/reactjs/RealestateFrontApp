import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import Footer from "../../components/Footer/Footer";
import Topbar from "../../components/TopBar/Topbar";
import profile from "./profile.css";

const Profile = () => {
  const user = useSelector((state) => state.user?.currentUser);

  return (
    <>
      <Topbar />
      <div className="profileContainer">
        <div className="userInformation">
          <span className="userInformationSpan">
            <b>Username : </b> zahraa alnahhas
          </span>
          <span className="userInformationSpan">
            <b>Email : </b> zahraanahhas@gmail.com
          </span>
        </div>
        <div className="buttonsContainer">
          {user ? (
            <>
              <Link
                to="/MyOffers"
                style={{ color: "black", textDecoration: "none" }}>
                {" "}
                <button className="profileButton">My Offers</button>
              </Link>
              <Link
                to="/MyOffersAppoinment"
                style={{ color: "black", textDecoration: "none" }}>
                {" "}
                <button className="profileButton">My Offers Appoinment</button>
              </Link>
            </>
          ) : null}
          <Link
            to="/MyAppoinment"
            style={{ color: "black", textDecoration: "none" }}>
            {" "}
            <button className="profileButton">My Appoinment</button>
          </Link>
        </div>
      </div>
      <Footer />
    </>
  );
};

export default Profile;
