import React, { useState } from 'react'
import Footer from '../../components/Footer/Footer';
import Topbar from '../../components/TopBar/Topbar';
import Slider from '../../components/Slider/Slider';
import SearchBox from '../../components/SearchBox/SearchBox';
import searchOffer from "./searchOffer.css"
import SearchResult from '../../components/SearchResult/SearchResult';
const SearchOffer = () => {

  const [searchedOffers, setSearchedOffers] = useState([])
  const images = [
    {
      url: "https://www.pinnacleproperties4u.com/admin/images/1541134427_online-search-flats-in-calicut.jpg",
    },
    {
      url: "https://pacificacompanies.co.in/wp-content/uploads/2019/06/Why-should-you-have-11.jpg",
    },
  ];

  const handleCallback = (data) => {
    setSearchedOffers(data)
  }


  return (
      <>
      <Topbar />
      <Slider prop={images} />
      <div className='bodyContainer'>
        <SearchBox handleCallback={handleCallback}/>
      <br/>
        <SearchResult searchedOffers={searchedOffers}/>
      </div>
      
          <Footer/>
      </>
  )
}

export default SearchOffer