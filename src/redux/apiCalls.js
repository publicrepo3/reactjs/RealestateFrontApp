import { loginFailure, loginStart, loginSuccess , logout, register } from "./userRedux";
import { publicRequest } from "../requestMethods";

export const login = async (dispatch, user) => {
  dispatch(loginStart());
  try {
    const res = await publicRequest.post("CreateAccount", user);
      dispatch(loginSuccess(res.data));
      console.log(res)
  } catch (err) {
    dispatch(loginFailure());
  }
};

export const logoutUser = (dispatch)=> {
  dispatch(logout());
  
};

export const registerToLogin = (dispatch)=> {
  dispatch(register());
  
};
