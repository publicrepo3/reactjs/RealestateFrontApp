import { createSlice } from "@reduxjs/toolkit";

const userSlice = createSlice({
  name: "seen",
  initialState: {
    currentState: false,
    isFetching: false,
    error: false,
  },
  reducers: {
    seenLoginStart: (state) => {
      state.isFetching = true;
    },
    seenLoginSuccess: (state) => {
      state.isFetching = false;
      state.currentState = true;
    },
    seenLoginFailure: (state) => {
      state.isFetching = false;
      state.error = true;
    },
    seenLoginEnded: (state) => {
      state.currentState = false;
      state.isFetching= false;
      state.error= false;
    },
    },
  },
);

export const { seenLoginStart, seenLoginSuccess, seenLoginFailure , seenLoginEnded } = userSlice.actions;
export default userSlice.reducer;
