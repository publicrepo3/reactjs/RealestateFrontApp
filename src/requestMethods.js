import axios from "axios";

const BASE_URL = "https://realstateserverapp.azurewebsites.net/RealstateServerApp/";
//const BASE_URL = "http://192.168.1.101:60000/RealstateServerApp/";

const user = JSON.parse(localStorage.getItem("persist:root"))?.user;
const currentUser = user && JSON.parse(user).currentUser;
const TOKEN = currentUser?.token;

const config = {     
  headers: {
    'Content-Type': 'multipart/form-data',
    "Accept" : "*/*",
    "Accept-Encoding": "gzip, deflate, br",
  "Connection" : "keep-alive"
  }
}

export const publicRequest = axios.create({
  baseURL: BASE_URL,
});

export const userRequest = axios.create({
  baseURL: BASE_URL,
  headers: { token: `Bearer ${TOKEN}` },
  
});

export const userRequestWithMultiPart = axios.create({
  baseURL: BASE_URL,
  config
  
});

